package withoutIterator;

import iterator.Expense;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PiePlot;
import org.jfree.chart.title.LegendTitle;
import org.jfree.data.general.DefaultPieDataset;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.*;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class GUIWithoutIterator extends JFrame {
    private List<Expense> expensesList = new ArrayList<>();
    private JTextField descriptionField;
    private JTextField amountField;
    private JComboBox<String> categoryComboBox;
    private JButton addExpenseButton;
    private JButton viewExpensesButton;
    private JButton viewStatisticsButton;
    private JTable expensesTable;
    private JPanel expensesChart;

    public GUIWithoutIterator() {

        Expense expenseProduct = new Expense("Milk", 50.0, new Date(), "Product");
        Expense expenseVehicle = new Expense("Gasoline", 20.0, new Date(), "Vehicle");
        Expense expenseEntertainment = new Expense("Cinema", 30.0, new Date(), "Entertainment");
        expensesList.add(expenseProduct);
        expensesList.add(expenseVehicle);
        expensesList.add(expenseEntertainment);

        setTitle("Cost management");
        setSize(1000, 600);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLayout(new BorderLayout());

        JPanel expensePanel = new JPanel(new GridLayout(5, 2));
        descriptionField = new JTextField();
        amountField = new JTextField();
        categoryComboBox = new JComboBox<>();
        categoryComboBox.setPreferredSize(new Dimension(50, 50));

        addExpenseButton = new JButton("Add expense");
        viewExpensesButton = new JButton("Show expenses");
        viewStatisticsButton = new JButton("Show statistics");
        addExpenseButton.setPreferredSize(new Dimension(50, 50));
        viewExpensesButton.setPreferredSize(new Dimension(50, 50));
        viewStatisticsButton.setPreferredSize(new Dimension(50, 50));

        JButton closeButton = new JButton("Close");

        expensePanel.add(new JLabel("Name of product:"));
        expensePanel.add(descriptionField);
        expensePanel.add(new JLabel("Amount:"));
        expensePanel.add(amountField);
        expensePanel.add(new JLabel("Category:"));
        expensePanel.add(categoryComboBox);
        expensePanel.add(addExpenseButton);
        expensePanel.add(viewExpensesButton);
        expensePanel.add(viewStatisticsButton);
        expensePanel.add(closeButton);

        JPanel tablePanel = new JPanel(new BorderLayout());
        expensesTable = new JTable();
        JScrollPane scrollPane = new JScrollPane(expensesTable);
        tablePanel.setPreferredSize(new Dimension(500, 300));
        tablePanel.add(scrollPane, BorderLayout.CENTER);

        JPanel chartPanel = new JPanel(new BorderLayout());
        expensesChart = new JPanel();
        JScrollPane scrollPane1 = new JScrollPane(expensesChart);
        chartPanel.setLayout(new BorderLayout());
        chartPanel.setPreferredSize(new Dimension(500, 300));
        chartPanel.add(scrollPane1, BorderLayout.CENTER);

        add(expensePanel, BorderLayout.NORTH);
        add(tablePanel, BorderLayout.WEST);
        add(chartPanel, BorderLayout.EAST);

        categoryComboBox.addItem("Product");
        categoryComboBox.addItem("Transport");
        categoryComboBox.addItem("Entertainment");


        closeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // Закрыть графический интерфейс
                dispose();
                System.exit(0);
            }
        });

        addExpenseButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String description = descriptionField.getText();
                double amount = Double.parseDouble(amountField.getText());
                String category = (String) categoryComboBox.getSelectedItem();
                if (category != null) {
                    Expense expense = new Expense(description, amount, new Date(), category);
                    expensesList.add(expense);
                    JOptionPane.showMessageDialog(GUIWithoutIterator.this, "The expense was added successfully!");
                    // Update table model
                    updateTableModel();
                    // Clear input fields
                    descriptionField.setText("");
                    amountField.setText("");
                } else {
                    JOptionPane.showMessageDialog(GUIWithoutIterator.this, "First, choose the category!");
                }
            }
        });

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");

        viewExpensesButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                DefaultTableModel model = new DefaultTableModel();
                model.addColumn("Name of product");
                model.addColumn("Amount");
                model.addColumn("Date");
                model.addColumn("Category");

                JTableHeader header = expensesTable.getTableHeader();
                header.setFont(header.getFont().deriveFont(Font.BOLD, 14f));
                header.setBackground(Color.LIGHT_GRAY);
                header.setForeground(Color.BLACK);
                DefaultTableCellRenderer renderer = (DefaultTableCellRenderer) expensesTable.getTableHeader().getDefaultRenderer();
                renderer.setHorizontalAlignment(JLabel.CENTER);

                for (Expense expense : expensesList) {
                    Object[] row = {
                            expense.getDescription(),
                            expense.getAmount(),
                            dateFormat.format(expense.getDate()),
                            expense.getCategory()};
                    model.addRow(row);
                }
                expensesTable.setModel(model);
            }
        });

        viewStatisticsButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                DefaultPieDataset dataset = new DefaultPieDataset();
                DefaultTableModel model = (DefaultTableModel) expensesTable.getModel();

                for (int i = 0; i < model.getRowCount(); i++) {
                    String category = (String) model.getValueAt(i, 3);
                    double amount = (double) model.getValueAt(i, 1);
                    if (dataset.getKeys().contains(category)) {
                        double oldValue = dataset.getValue(category).doubleValue();
                        dataset.setValue(category, oldValue + amount);
                    } else {
                        dataset.setValue(category, amount);
                    }
                }

                JFreeChart chart = ChartFactory.createPieChart(
                        "Cost statistics by category",
                        dataset,
                        true,
                        true,
                        false);

                PiePlot plot = (PiePlot) chart.getPlot();
                Font labelFont = plot.getLabelFont();
                Font newLableFont = labelFont.deriveFont(labelFont.getSize() * 1.2f);

                Font titleFont = chart.getTitle().getFont();
                Font newTitleFont = titleFont.deriveFont(titleFont.getSize() * 0.8f);
                chart.getTitle().setFont(newTitleFont);

                LegendTitle legend = chart.getLegend();
                Font legendFont = legend.getItemFont();
                Font newLegendFont = legendFont.deriveFont(legendFont.getSize() * 1.5f);
                legend.setItemFont(newLegendFont);

                plot.setLabelFont(newLableFont);
                plot.setBackgroundPaint(Color.WHITE);

                plot.setLabelFont(newLableFont);
                plot.setBackgroundPaint(Color.WHITE);
                plot.setSectionPaint("Product", Color.pink);
                plot.setSectionPaint("Vehicle", Color.blue);
                plot.setSectionPaint("Entertainment", Color.orange);

                ChartPanel chartPanel = new ChartPanel(chart);
                chartPanel.setPreferredSize(new Dimension(450, 250));
                expensesChart.setLayout(new BorderLayout());
                expensesChart.removeAll();
                expensesChart.add(chartPanel, BorderLayout.CENTER);
                expensesChart.revalidate();
                expensesChart.repaint();
            }
        });
    }

    private void updateTableModel() {
        DefaultTableModel model = (DefaultTableModel) expensesTable.getModel();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
        // Clear existing rows
        model.setRowCount(0);
        // Add expenses to the table model
        for (Expense expense : expensesList) {
            Object[] row = {
                    expense.getDescription(),
                    expense.getAmount(),
                    dateFormat.format(expense.getDate()),
                    expense.getCategory()
            };
            model.addRow(row);
        }
    }


    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                GUIWithoutIterator gui = new GUIWithoutIterator();
                gui.setVisible(true);
            }
        });
    }
}