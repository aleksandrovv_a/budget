package withoutIterator;

import iterator.Expense;
import iterator.Expenses;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        //создание коллекции расходов
        // Создание коллекции расходов
        Expenses expenses = new Expenses();

        // Добавление категорий
        expenses.addCategory("Продукты");
        expenses.addCategory("Транспорт");
        expenses.addCategory("Развлечения");

        // Добавление расходов
        expenses.addExpense("Молоко", 50.0, new Date(), "Продукты");
        expenses.addExpense("Бензин", 20.0, new Date(), "Транспорт");
        expenses.addExpense("Кино", 30.0, new Date(), "Развлечения");

        // Получение расходов за определенный период времени
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_MONTH, -7); // 7 дней назад
        Date startDate = calendar.getTime();
        Date endDate = new Date(); // Текущая дата

        // Получение списка категорий расходов
        List<Expense> foodExpenses = expenses.getExpensesByCategory("Продукты");
        System.out.println("Расходы на продукты:");
        for (Expense expense : foodExpenses) {
            System.out.println("Описание: " + expense.getDescription() + ", Сумма: " + expense.getAmount() + ", Дата: " + expense.getDate());
        }

        // Использование итератора для перебора расходов и вывода информации о каждом расходе
        List<Expense> expensesForPeriod = expenses.getExpensesForPeriod(startDate, endDate);
        System.out.println("Расходы за период с " + startDate + " по " + endDate + ":");
        for (Expense expense : expensesForPeriod) {
            System.out.println("Категория: " + expense.getCategory() + ", Сумма: " + expense.getAmount() + ", Дата: " + expense.getDate());
        }
    }
}