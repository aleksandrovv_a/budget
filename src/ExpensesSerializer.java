import iterator.Expenses;

import java.io.*;

public class ExpensesSerializer {
    private static final String FILENAME = "expenses.ser";

    public static void saveExpenses(Expenses expenses) {
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(FILENAME))) {
            oos.writeObject(expenses);
            System.out.println("iterator.Expenses saved successfully");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Expenses loadExpenses() {
        Expenses expenses = null;
        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(FILENAME))) {
            expenses = (Expenses) ois.readObject();
            System.out.println("iterator.Expenses loaded successfully");
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return expenses;
    }
}
