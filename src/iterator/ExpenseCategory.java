package iterator;

import iterator.Expense;

import java.util.ArrayList;
import java.util.List;

public class ExpenseCategory {
    private List<Expense> expenses;
    public ExpenseCategory() {
        expenses = new ArrayList<>();
    }
    public void addExpense(Expense expense) {
        expenses.add(expense);
    }
    public List<Expense> getExpenses() {
        return expenses;
    }
}
