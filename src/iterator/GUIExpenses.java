package iterator;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PiePlot;
import org.jfree.chart.title.LegendTitle;
import org.jfree.data.general.DefaultPieDataset;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;


public class GUIExpenses extends JFrame {
    private Expenses expenses;
    private JTextField descriptionField;
    private JTextField amountField;
    private JComboBox<String> categoryComboBox;
    private JButton addExpenseButton;
    private JButton viewExpensesButton;
    private JButton viewStatisticsButton;
    private JTable expensesTable;
    private JPanel expensesChart;

    public GUIExpenses(Expenses expenses) {
        this.expenses = expenses;

//        expenses = new iterator.Expenses();
        // Настройка основного окна
        setTitle("Cost management");
        setSize(1000, 600);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLayout(new BorderLayout());

        // Панель для ввода нового расхода
        JPanel expensePanel = new JPanel(new GridLayout(5, 2));
        descriptionField = new JTextField();
        amountField = new JTextField();
        categoryComboBox = new JComboBox<>();
        categoryComboBox.setPreferredSize(new Dimension(50, 50));

        addExpenseButton = new JButton("Add expense");
        viewExpensesButton = new JButton("Show expenses");
        viewStatisticsButton = new JButton("Show statistics");
        addExpenseButton.setPreferredSize(new Dimension(50, 50));
        viewExpensesButton.setPreferredSize(new Dimension(50, 50));
        viewStatisticsButton.setPreferredSize(new Dimension(50, 50));
//        expensePanel.setPreferredSize(new Dimension(50, 250));

        expensePanel.add(new JLabel("Name of product:"));
        expensePanel.add(descriptionField);
        expensePanel.add(new JLabel("Amount:"));
        expensePanel.add(amountField);
        expensePanel.add(new JLabel("Category:"));
        expensePanel.add(categoryComboBox);
        expensePanel.add(addExpenseButton);
        expensePanel.add(viewExpensesButton);
        expensePanel.add(viewStatisticsButton);

        JPanel tablePanel = new JPanel(new BorderLayout());
        expensesTable = new JTable();
        JScrollPane scrollPane = new JScrollPane(expensesTable);
        tablePanel.setPreferredSize(new Dimension(500, 300));
        tablePanel.add(scrollPane, BorderLayout.CENTER);

        JPanel chartPanel = new JPanel(new BorderLayout());
        expensesChart = new JPanel();
        JScrollPane scrollPane1 = new JScrollPane(expensesChart);
        chartPanel.setLayout(new BorderLayout());
        chartPanel.setPreferredSize(new Dimension(500, 300));
        chartPanel.add(scrollPane1, BorderLayout.CENTER);

        add(expensePanel, BorderLayout.NORTH);
        add(tablePanel, BorderLayout.WEST);
        add(chartPanel, BorderLayout.EAST);

        // Добавление категорий в коллекцию расходов
        expenses.addCategory("Product");
        expenses.addCategory("Transport");
        expenses.addCategory("Entertainment");

        // Заполнение выпадающего списка категориями из коллекции расходов
        Set<String> categories = expenses.getCategories();
        for (String category : categories) {
            categoryComboBox.addItem(category);
        }
        // Обработчик кнопки "Добавить расход"
        addExpenseButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String description = descriptionField.getText();
                double amount = Double.parseDouble(amountField.getText());
                String category = (String) categoryComboBox.getSelectedItem();
                if (category != null) {
                    expenses.addExpense(description, amount, new Date(), category);
                    JOptionPane.showMessageDialog(GUIExpenses.this, "The expense was added successfully!");
                } else {
                    JOptionPane.showMessageDialog(GUIExpenses.this, "First, choose the category!");
                }
            }
        });

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.YYYY");
        // Обработчик кнопки "Просмотр расходов"
        viewExpensesButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                Date endDate = new Date();
                Date startDate = new Date(endDate.getTime() - 7 * 24 * 60 * 60 * 1000); // 7 дней назад

                DefaultTableModel model = new DefaultTableModel();
                model.addColumn("Name of product");
                model.addColumn("Amount");
                model.addColumn("Date");
                model.addColumn("Category");

                // Get header of table
                JTableHeader header = expensesTable.getTableHeader();
                header.setFont(header.getFont().deriveFont(Font.BOLD, 14f)); // Установка размера и жирного стиля шрифта

                header.setBackground(Color.LIGHT_GRAY);
                header.setForeground(Color.BLACK);

                DefaultTableCellRenderer renderer = (DefaultTableCellRenderer) expensesTable.getTableHeader().getDefaultRenderer();
                renderer.setHorizontalAlignment(JLabel.CENTER);

                for (Expense expense : expenses.getExpensesForPeriod(startDate, endDate)) {
                    Object[] row = {
                            expense.getDescription(),
                            expense.getAmount(),
                            dateFormat.format(expense.getDate()),
                            expense.getCategory()};
                    model.addRow(row);
                }
                expensesTable.setModel(model);
            }
        });

        viewStatisticsButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // Генерация статистики
                generateStatistics();
            }
        });
    }

    // Method for generating statistic and make pieChart
    private void generateStatistics() {
        DefaultPieDataset dataset = new DefaultPieDataset();
        DefaultTableModel model = (DefaultTableModel) expensesTable.getModel();

        for (int i = 0; i < model.getRowCount(); i++) {
            // Get category and amount from table
            String category = (String) model.getValueAt(i, 3);
            double amount = (double) model.getValueAt(i, 1);
            // Update dataset with category and amount
            if (dataset.getKeys().contains(category)) {
                double oldValue = dataset.getValue(category).doubleValue();
                dataset.setValue(category, oldValue + amount);
            } else {
                dataset.setValue(category, amount);
            }
        }

        // Create the chart
        JFreeChart chart = ChartFactory.createPieChart(
                "Cost statistics by category",
                dataset,
                true, // include legend
                true,
                false);



        PiePlot plot = (PiePlot) chart.getPlot();
        Font labelFont = plot.getLabelFont();
        Font newLableFont = labelFont.deriveFont(labelFont.getSize() * 1.2f);

        Font titleFont = chart.getTitle().getFont();
        Font newTitleFont = titleFont.deriveFont(titleFont.getSize() * 0.8f);
        chart.getTitle().setFont(newTitleFont);

        LegendTitle legend = chart.getLegend();
        Font legendFont = legend.getItemFont();
        Font newLegendFont = legendFont.deriveFont(legendFont.getSize() * 1.5f);
        legend.setItemFont(newLegendFont);

        plot.setLabelFont(newLableFont);
        plot.setBackgroundPaint(Color.WHITE);
        plot.setSectionPaint("Product", Color.blue);
        plot.setSectionPaint("Vehicle", Color.LIGHT_GRAY);
        plot.setSectionPaint("Entertainment", Color.BLACK);

        // Update the chart panel
        ChartPanel chartPanel = new ChartPanel(chart);
        chartPanel.setPreferredSize(new Dimension(450, 250));
        this.expensesChart.setLayout(new BorderLayout());
        this.expensesChart.removeAll();
        this.expensesChart.add(chartPanel, BorderLayout.CENTER);
        this.expensesChart.revalidate();
        this.expensesChart.repaint();
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                Expenses expenses = new Expenses();
                GUIExpenses gui = new GUIExpenses(expenses);
                gui.setVisible(true);

                ExpenseIterator iterator = expenses.expenseIterator();
                while (iterator.hasNext()) {
                    Expense expense = iterator.nextExpense();
                    System.out.println(expense.getDescription());
                }
            }
        });
    }
}