package iterator;

public interface ExpenseIterator {
    ExpenseIterator expenseIterator();

    boolean hasNext();
    Expense nextExpense();
}
