package iterator;

import java.util.*;

//класс для коллекции расходов
public class Expenses implements ExpenseIterator {
    private List<Expense> expenseList;
    private Map<String, ExpenseCategory> categoryMap;
    private double budget;

    public Expenses() {
        expenseList = new ArrayList<>();
        categoryMap = new HashMap<>();
        budget = 0.0;

        // Добавление некоторых начальных расходов
        addExpense("Milk", 50.0, new Date(), "Product");
        addExpense("Gasoline", 20.0, new Date(), "Vehicle");
        addExpense("Cinema", 30.0, new Date(), "Entertainment");
    }

    public void addExpense(String description, double amount, Date date, String category) {
        Expense expense = new Expense(description, amount, date, category);
        expenseList.add(expense);
        categoryMap.computeIfAbsent(category, k -> new ExpenseCategory()).addExpense(expense);
    }

    public void addCategory(String category) {
        categoryMap.put(category, new ExpenseCategory());
    }

    public ExpenseCategory getCategoryByName(String category) {
        return categoryMap.get(category);
    }

    public Set<String> getCategories() {
        return categoryMap.keySet();
    }

    public List<Expense> getExpensesForPeriod(Date startDate, Date endDate) {
        List<Expense> expensesForPeriod = new ArrayList<>();
        for (Expense expense : expenseList) {
            if (expense.getDate().after(startDate) && expense.getDate().before(endDate)) {
                expensesForPeriod.add(expense);
            }
        }
        return expensesForPeriod;
    }

    public List<Expense> getExpensesByCategory(String category) {
        ExpenseCategory expenseCategory = categoryMap.get(category);
        return (expenseCategory != null) ? expenseCategory.getExpenses() : new ArrayList<>();
    }

    public double getTotalAmountForPeriod(Date startDate, Date endDate) {
        double totalAmount = 0.0;
        for (Expense expense : expenseList) {
            if (expense.getDate().after(startDate) && expense.getDate().before(endDate)) {
                totalAmount += expense.getAmount();
            }
        }
        return totalAmount;
    }

    public Map<String, Double> getCategoryWiseAmountForPeriod(Date startDate, Date endDate) {
        Map<String, Double> categoryWiseAmount = new HashMap<>();
        for (Expense expense : expenseList) {
            if (expense.getDate().after(startDate) && expense.getDate().before(endDate)) {
                String category = expense.getCategory();
                double amount = expense.getAmount();
                categoryWiseAmount.put(category, categoryWiseAmount.getOrDefault(category, 0.0) + amount);
            }
        }
        return categoryWiseAmount;
    }

    public void setBudget(double budget) {
        this.budget = budget;
    }

    public double getBudget() {
        return budget;
    }

    public boolean isBudgetExceeded(Date startDate, Date endDate) {
        double totalAmount = getTotalAmountForPeriod(startDate, endDate);
        return totalAmount > budget;
    }

    public List<Expense> getExpenseList() {
        return expenseList;
    }

//    @Override
//    public Iterator<iterator.Expense> iterator() {
//        return new ExpenseIteratorImpl();
//    }

    @Override
    public ExpenseIterator expenseIterator() {
        return new ExpenseIteratorImpl();
    }

    @Override
    public boolean hasNext() {
        return false;
    }

    @Override
    public Expense nextExpense() {
        return null;
    }

    // Приватный внутренний класс
    private class ExpenseIteratorImpl implements ExpenseIterator {
        private int currentIndex = 0;

        @Override
        public ExpenseIterator expenseIterator() {
            return null;
        }

        @Override
        public boolean hasNext() {
            return currentIndex < expenseList.size();
        }

        @Override
        public Expense nextExpense() {
            if (!hasNext()) {
                throw new NoSuchElementException();
            }
            return expenseList.get(currentIndex++);
        }
    }
}